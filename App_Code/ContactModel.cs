﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ContactModel
/// </summary>
public class ContactModel
{
    public string name { get; set; }
    public string email { get; set; }
    public string message { get; set; }

    public ContactModel()
    {


    }
}