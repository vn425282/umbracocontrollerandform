﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Web.Mvc;

/// <summary>
/// Summary description for ContactSurfaceController
/// </summary>
namespace Umbraco7
{
    public class ContactSurfaceController : SurfaceController
    {
        public ActionResult ShowForm()
        {
            return PartialView("ContactForm", new ContactModel());
        }

        public ActionResult HandleFormPost(ContactModel model)
        {
            var newMessage = Services.ContentService.CreateContent(model.name + " " + DateTime.Now.ToString("dd-MM-yyyy HH:mm"), CurrentPage.Id, "contactFormula");
            newMessage.SetValue("emailFrom", model.email);
            newMessage.SetValue("contactName", model.name);
            newMessage.SetValue("contactDescription", model.message);
            Services.ContentService.SaveAndPublishWithStatus(newMessage);
            return RedirectToCurrentUmbracoPage();
        }
    }
}