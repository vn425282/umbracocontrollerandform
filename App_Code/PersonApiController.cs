﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Persistence;
using Umbraco.Web.Editors;

/// <summary>
/// Summary description for PersonApiController
/// </summary>
[Umbraco.Web.Mvc.PluginController("My")]
public class PersonApiController : UmbracoAuthorizedJsonController
{
    public IEnumerable<Person> GetAll()
    {
        //get the database
        var db = UmbracoContext.Application.DatabaseContext.Database;
        //build a query to select everything the people table
        var query = new Sql().Select("*").From("people");
        //fetch data from DB with the query and map to Person object
        return db.Fetch<Person>(query);
    }
}

public class Person
{
    public int Id { get; set; }
    public string Name { get; set; }
    public string Town { get; set; }
    public string Country { get; set; }
}